@extends('layout')

@section('content')
<div class="container ">

    <div class="row padding-top">
   
        <div class="col-lg-3" >        
            <div class="row">
                  <div class="col-lg-12 sidebar">
                     <ul type="none">
                     <li><h6 class="top_margin">Related Colleges</h6></li>
                     @foreach($clg_list as $clg_list)
                        <li><a href="college_detail/{{ $clg_list->city_id }}/{{ $clg_list->id }}">{{ $clg_list->college_name }}</a> </li>
                        @endforeach  
                     </ul>
                  </div>
           </div>
        </div>

        <div class="col-lg-9" >
          <div class="row show-box" >
            <div class="col-lg-12" >
             <h1>College Detail</h1>
            </div>
            <div class="col-lg-12">
             <table>
             @foreach($detail as $detail)
             <tr>
                <th>College name</th>
                <td><a href="college_detail/{{ $detail->city_id }}/{{ $detail->id }}">{{ $detail->college_name }} </a></td>
             </tr>
             <tr>
                <th>Address</th>
                <td> {{ $detail->address }} </td>
             </tr>
             <tr>
                <th>Phone no</th>
                <td> {{ $detail->phone_no }}</td>
             </tr>
             <tr>
                <th>Website</th>
                <td> {{ $detail->web_url }}</td>
             </tr>
             <tr>
                <th>University</th>
                <td> {{ $detail->university }}</td>
             </tr>
             <tr>
                <th>Contact Person</th>
                <td> {{ $detail->contact_person }}</td>
             </tr>
             @endforeach
             </table>
            </div>
          </div>
        </div>
    </div>  
 </div>
@endsection()