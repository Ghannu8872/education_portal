@extends('layout')
@section('content')
<div class="container">

    <div class="row ">
    @isset($message)
    <div class="alert alert-success">
    <strong>{{$message}}</strong>
    </div>
    @endif

        <div class="col-sm-12" >
          <div class="row edit-box" >
            <div class="col-sm-12" >
             <h1>All College </h1>
             <a class="btn btn-success" href="/add-college">Add Course</a> 
            </div>
            <div class="col-sm-12">
             <table border=1>
           
             <tr>
                <th class="td-padding col-width">College name</th>
                <th class="td-padding col-width">Address</th>
                <th class="td-padding col-width">Phone no</th>
                <th class="td-padding col-width">Website</th>
                <th class="td-padding col-width">University</th>
                <th class="td-padding col-width">Contact Person</th>
                <th class="td-padding col-width" colspan=2 >Action</th>
             </tr>
             @foreach($colleges as $colleges)
             <tr>
                 <td class="td-padding">{{ $colleges->college_name }} </td>
                <td class="td-padding"> {{ $colleges->address }} </td>
                <td class="td-padding"> {{ $colleges->phone_no }}</td>
                <td class="td-padding"> {{ $colleges->web_url }}</td>
                <td class="td-padding"> {{ $colleges->university }}</td>
                <td class="td-padding"> {{ $colleges->contact_person }}</td>                
                <td class="td-padding">
                <a href="{{ route('clg.edit',$colleges->id)}}" class="btn btn-primary">Edit</a>
                </td>
                <td>
                <form action="{{ route('clg.delete', $colleges->id)}}" method="post">
                  @csrf
                  {{ method_field('POST') }} 
                  <button class="btn btn-danger" type="submit">Delete</button>
                </form>
                 </td>
             </tr>            
             @endforeach
             </table>
            </div>
          </div>
        </div>
    </div>  
 </div>
@endsection()