
@extends('layout')

@section('content')
<div class="row">
 <div class="col-sm-8 offset-sm-2">
    <h1 class="display-3">Add College Detail</h1>
  <div>
    @if ($errors->any())
      <div class="alert alert-danger">
        <ul>
            @foreach ($errors->all() as $error)
              <li>{{ $error }}</li>
            @endforeach
        </ul>
      </div><br />
    @endif
      <form method="post" action="{{ route('clg.store') }}">
          @csrf
          <div class="form-group"> 
              <div class="row">
                  
                  <div class="col-sm-4">
                   <label for="title">Select Country:</label>
                      <select id="country" name="country" class="form-control" required style="width:250px; height:50px" >
                          <option value="" selected disabled>-Select Country-</option>
                          @foreach($countries as $key => $country)
                          <option value="{{$key}}||{{ old('country') }}"> {{$country}}</option>
                      @endforeach
                      </select>
                    </div>
  
                        <div class="col-sm-4">
                        <label for="title">Select State:</label>
                          <select name="state" id="state" class="form-control" required style="width:250px; height:50px">
                          <option value='' selected>-Select State-</option>
                         
                         </select>
                        </div>
  
                        <div class="col-sm-4">
                        <label for="title">Select City:</label>
                       <select name="city" id="city" class="form-control" required style="width:250px; height:50px">
                       <option value='' selected>-Select City-</option>
                       </select>
                        </div>
                        
                    </div>   

            <div>
              <label for="college_name">College Name</label>
              <input type="text" placeholder="College Name" class="form-control" name="college_name" required value="{{ old('college_name') }}" />

          </div>
          <div class="form-group">
              <label for="University">University:</label>
              <input type="text" placeholder="University" class="form-control" name="university" required value="{{ old('university') }}" />
          </div> 

          <div class="form-group">
              <label for="address">Address:</label>
              <textarea type="text" placeholder="Address" class="form-control" name="address" required value="{{ old('address') }}" ></textarea>
          </div>
          <div class="form-group">
              <label for="phone_no">Phone no:</label>
              <input type="text" placeholder="Phone no" class="form-control" name="phone_no" required value="{{ old('phone_no') }}" />
          </div>

          <div class="form-group">
              <label for="web_url">Website:</label>
              <input type="text" placeholder="Website" class="form-control" name="web_url" required value="{{ old('web_url') }}" />
          </div>
           
          <div class="form-group">
              <label for="contact_person">Contact Person:</label>
              <input type="text" placeholder="Contact Person" class="form-control" name="contact_person" required value="{{ old('contact_person') }}" />
          </div>                      
          <button type="submit" class="btn btn-primary">Submit</button>
      </form>
  </div>
</div>
</div>
@endsection

@section('script')
<script type="text/javascript">


//  Get State List
    $('#country').change(function(){
        var old_countryID = '{{ old('countryID') }}';
            if(old_countryID !== '') {
            $('#country').val(old_countryID );
            }
    var countryID = $(this).val();  
    // alert(countryID);
    if(countryID){
        $.ajax({
           type:"GET",
           url:"{{url('get-state-list')}}?country_id="+countryID,
           success:function(res){               
            if(res){
                $("#state").empty();
                var options ="<option value='' selected>-Select State-</option>";
                $.each(res,function(key,value){
                    options +='<option value="'+key+'" >'+value+'</option>';
                   
                    $("#state").append(options);
                });
           
            }else{
               $("#state").empty();
             
            }
           }
        });
    }else{
        $("#state").empty();
        $("#city").empty();
    }      
   });

   //  Get City List
    $('#state').on('change',function(){


    var stateID = $(this).val();    
    if(stateID){
        $.ajax({
           type:"GET",
           url:"{{url('get-city-list')}}?state_id="+stateID,
           success:function(res){               
            if(res){
                $("#city").empty();
                var options ="<option value='' selected>-Select State-</option>";
                $.each(res,function(key,value){
                    options +='<option value="'+key+'" >'+value+'</option>';
                    $("#city").append(options);
                });
           
            }else{
               $("#city").empty();
            }
           }
        });
    }else{
        $("#city").empty();
    }
        
   });
//  Get College List
   $('#city').on('change',function(){
    
    var cityID = $(this).val();    
    if(cityID){
        $.ajax({
           type:"GET",
           url:"{{url('get-college-list')}}?city_id="+cityID,
           success:function(res){               
            if(res){
            }else{
               $("#city").empty();
            }
           }
        });
    }else{
        $("#city").empty();
    }
        
   });

</script>

@endsection()