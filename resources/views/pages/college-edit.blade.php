
@extends('layout')

@section('content')
<div class="row">
 <div class="col-sm-8 offset-sm-2">
    <h1 class="display-3">Edit College Detail</h1>
  <div>
    @if ($errors->any())
      <div class="alert alert-danger">
        <ul>
            @foreach ($errors->all() as $error)
              <li>{{ $error }}</li>
            @endforeach
        </ul>
      </div><br />
    @endif
    @foreach($colleges as $colleges)
    
      <form method="post" action="{{ route('clg.update', $colleges->id) }}">
          @csrf
          <div class="form-group"> 
           
            <div>
              <label for="college_name">College Name</label>

              <input type="text" class="form-control" name="college_name" required value="{{ $colleges->college_name }}" />

          </div>
          <div class="form-group">
              <label for="University">University:</label>
              <input type="text" class="form-control" name="university" required value="{{ $colleges->university }}" />
          </div> 

          <div class="form-group">
              <label for="address">Address:</label>
              <textarea type="text" class="form-control" name="address" required value="" >{{ $colleges->address }}</textarea>
          </div>
          <div class="form-group">
              <label for="phone_no">Mobile no:</label>
              <input type="tel" class="form-control" name="phone_no" required value="{{ $colleges->phone_no }}" />
          </div>

          <div class="form-group">
              <label for="web_url">Website Address:</label>
              <input type="text" class="form-control" name="web_url" required value="{{ $colleges->web_url }}" />
          </div>
           
          <div class="form-group">
              <label for="contact_person">Contact Person:</label>
              <input type="text" class="form-control" name="contact_person" required value="{{ $colleges->contact_person }}" />
          </div>                      
          <button type="submit" class="btn btn-primary">Update</button>
      </form>
      @endforeach
  </div>
</div>
</div>
@endsection
