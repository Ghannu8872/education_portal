
@extends('layout')

@section('content')
<div class="container">
<!-- @include('include/search') -->
    <div class="row " style="height:auto">
        <div class="col-sm-3" >        
            <div class="row">
                  <div class="col-sm-12 sidebar">
                     <ul type="none">
                     <li><h6 class="top_margin">Related Colleges</h6></li>
                     @foreach($clg_list as $clg_list)
                        <li><a href="/college_detail/{{ $clg_list->city_id }}/{{ $clg_list->id }}">{{ $clg_list->college_name }}</a> </li>
                        @endforeach  
                     </ul>
                  </div>
           </div>
        </div>
        <div class="col-sm-9" style="height:auto">
        <div  class="row" >
             <div class="col-sm-12" >
             <h3>Sponsored Link</h3>
            </div>
            <div class="col-sm-12">
            <table>
             @foreach($clg_detail as $clg_detail)
             <tr>
                <th>College name</th>
                <td style="color:blue; font-weight: bold;}">{{ $clg_detail->college_name }}</td>
             </tr>
             <tr>
                <th>Address</th>
                <td> {{ $clg_detail->address }} </td>
             </tr>
             <tr>
                <th>Phone no</th>
                <td> {{ $clg_detail->phone_no }}</td>
             </tr>
             <tr>
                <th>Website</th>
                <td> {{ $clg_detail->web_url }}</td>
             </tr>
             @endforeach
             </table>
            </div>  
         </div>    
        </div>
    </div>   
    <div class="row">
       <div class="col-lg-3">

       </div>
      <div class="col-lg-9">
        <div class="wow fadeInLeft">
           
         <div class="row course-box-border" >

            <div class="col-lg-12 course-heading">
            <ul type="none" >
                <li class="left course-padding tablink select-tab" onclick="open_course(event,'commerce')"><a>Commerce</a></li>
                <li class="left course-padding tablink" onclick="open_course(event,'management')"><a>Management</a></li>
            </ul>
            </div>
            
            <div class="col-lg-12" style="min-height:300px">
            <ul id="commerce" type="none" class="course"  >
                @foreach($commerce_list as $commerce_list)
                <li class="padding"><a href="{{ route('course.info',$commerce_list->id)}}">{{ $commerce_list->course }}</a></li>
                @endforeach
            </ul>

            <ul id="management" type="none"  class="course" style="display:none">
               @foreach($management_list as $management_list)
                <li class="padding"><a href="{{ route('course.info',$management_list->id)}}">{{ $management_list->course }}</a></li>
                @endforeach
            </ul>
            </div >
          </div> 

         </div>
      </div>
   </div>
 </div>
 @include('pages/contact_to_us') 

 <script>
function open_course(evt, course_name) {
   var i, x, tablinks;
  x = document.getElementsByClassName("course");
  for (i = 0; i < x.length; i++) {
    x[i].style.display = "none";  
  }
  tablinks = document.getElementsByClassName("tablink");
  for (i = 0; i < x.length; i++) {
    tablinks[i].className = tablinks[i].className.replace("select-tab", ""); 
  }

  document.getElementById(course_name).style.display = "block";  
  evt.currentTarget.className += " select-tab";

}
</script>
<!-- <script type="text/javascript">
    $('#commerce').on('keyup',function(){
      //   var collegeID = $("select[name=search]").val();
      alert("Hellow");
        $.ajax({
        type : 'get',
        url : '{{URL::to('get-commerce-course')}}',
        data:{             
                'courses': $('#commerce').val()
        },
        success:function(data){
         $('ul').html(data);
        }
        });
        })
    </script> -->
    
@endsection()