
@extends('layout')

@section('content')
<div class="row">
 <div class="col-sm-8 offset-sm-2">
    <h1 class="display-3">Add Course Detail</h1>
  <div>
    @if ($errors->any())
      <div class="alert alert-danger">
        <ul>
            @foreach ($errors->all() as $error)
              <li>{{ $error }}</li>
            @endforeach
        </ul>
      </div><br />
    @endif
      <form method="post" action="{{ route('course.store') }}" >
          @csrf
          <div class="form-group"> 
              <div class="row">
                  
                  <div class="col-sm-3">
                   <label for="title">Select Country:</label>
                      <select id="country" required name="country" class="form-control" required style="width:250px; height:50px" >
                          <option value="" selected disabled>-Select Country-</option>
                          @foreach($countries as $key => $country)
                          <option value="{{$key}}||{{ old('country') }}"> {{$country}}</option>
                      @endforeach
                      </select>
                    </div>
  
                        <div class="col-sm-3">
                        <label for="title">Select State:</label>
                          <select name="state" required id="state" class="form-control" required style="width:250px; height:50px">
                          <option value='' selected>-Select State-</option>
                         
                         </select>
                        </div>
  
                        <div class="col-sm-3">
                        <label for="title">Select City:</label>
                       <select name="city" required id="city" class="form-control" required style="width:250px; height:50px">
                       <option value='' selected>-Select City-</option>
                       </select>
                        </div>

                        <div class="col-lg-3">
                      <label for="title">Select College:</label>
                      <select name="college_id" required id="college_id" class="form-control" style="width:300px; height:50px">
                  
                        </select>                        
                      </div>
                        
                    </div>   

            <div>
              <label for="Stream">Stream :</label>              
              <select name="stream" class="form-control" required   style="height:40px">
                  <option value="" >-Select Stream-</option>
                  <option value="commerce" {{ old('stream') == 'commerce' ? 'selected' : '' }}>Commerce</option>
                  <option value="management" {{ old('stream') == 'management' ? 'selected' : '' }}>Management</option>
               
             </select> 
          </div>
          <div>
              <label for="course">Course :</label>
              <input type="text" required placeholder="Course" class="form-control" name="course"/>
             
            </div>
          <div>
              <label for="cost">Cost :</label>
              <input type="text" placeholder="Cost" class="form-control" name="cost" required value="{{ old('cost') }}" />
          </div>
          <div class="form-group">
              <label for="duration">Duration:</label>
            
              <select name="duration" class="form-control"required value="{{ old('duration') }}" style="height:40px">
                <option value="" selected>-Select Duration-</option>
                <option value="1 year"  {{ old('duration') == '1 year' ? 'selected' : '' }}>1 year</option>
                <option value="2 year" {{ old('duration') == '2 year' ? 'selected' : '' }}>2 year</option>
                <option value="3 year" {{ old('duration') == '3 year' ? 'selected' : '' }}>3 year</option>
                <option value="4 year" {{ old('duration') == '4 year' ? 'selected' : '' }}>4 year</option>
                <option value="5 year" {{ old('duration') == '5 year' ? 'selected' : '' }}>5 year</option>
             
           </select>
            </div> 

          
          <div class="form-group">
              <label for="eligibility">Eligibility:</label>
              <input type="text" placeholder="Eligibility" class="form-control" name="eligibility" required value="{{ old('eligibility') }}" />
          </div>
          <div class="form-group">
              <label for="address">Address:</label>
              <textarea type="text" placeholder="Address" class="form-control" name="address" required value="{{ old('address') }}" ></textarea>
          </div>
          <div class="form-group">
              <label for="phone_no">Phone no:</label>
              <input type="text" placeholder="Phone no" class="form-control" name="phone_no" required value="{{ old('phone_no') }}" />
          </div> 
          <div class="form-group">
              <label for="web_url">Website :</label>
              <input type="text" placeholder="Website" class="form-control" name="web_url" required value="{{ old('web_url') }}" />
          </div>         
          <button type="submit" class="btn btn-primary">Submit</button>
      </form>
  </div>
</div>
</div>
@endsection

@section('script')
<script type="text/javascript">


//  Get State List
    $('#country').change(function(){
        var old_countryID = '{{ old('countryID') }}';
            if(old_countryID !== '') {
            $('#country').val(old_countryID );
            }
    var countryID = $(this).val();  
    // alert(countryID);
    if(countryID){
        $.ajax({
           type:"GET",
           url:"{{url('get-state-list')}}?country_id="+countryID,
           success:function(res){               
            if(res){
                $("#state").empty();
                var options ="<option value='' selected>-Select State-</option>";
                $.each(res,function(key,value){
                    options ='<option value="'+key+'" >'+value+'</option>';
                   
                    $("#state").append(options);
                });
           
            }else{
               $("#state").empty();
             
            }
           }
        });
    }else{
        $("#state").empty();
        $("#city").empty();
        $("#college_id").empty();
      
    }      
   });

   //  Get City List
    $('#state').on('change',function(){


    var stateID = $(this).val();    
    if(stateID){
        $.ajax({
           type:"GET",
           url:"{{url('get-city-list')}}?state_id="+stateID,
           success:function(res){               
            if(res){
                $("#city").empty();
                var options ="<option value='' selected>-Select State-</option>";
                $.each(res,function(key,value){
                    options ='<option value="'+key+'" >'+value+'</option>';
                    $("#city").append(options);
                });
           
            }else{
               $("#city").empty();
            }
           }
        });
    }else{
        $("#city").empty();
        $("#college_id").empty();
       
    }
        
   });
//  Get College List
   $('#city').on('change',function(){
    
    var cityID = $(this).val();    
    if(cityID){
        $.ajax({
           type:"GET",
           url:"{{url('get-college-list')}}?city_id="+cityID,
           success:function(res){               
            if(res){
                $("#college_id").empty();   
              
                var options ="<option value='' selected>-Select College-</option>";         
                $.each(res,function(key,value){   
                    options ='<option value="'+key+'" >'+value+'</option>';                   
                    $("#college_id").append(options);  
                });
            }else{
               $("#city").empty();
            }
           }
        });
    }else{
       
        $("#college_id").empty();
       
    }
        
   });


</script>

@endsection()