

@extends('layout')

@section('content')
<div class="row">
 <div class="col-sm-8 offset-sm-2">
    <h1 class="display-3">Edit Course Detail</h1>
  <div>
    @if ($errors->any())
      <div class="alert alert-danger">
        <ul>
            @foreach ($errors->all() as $error)
              <li>{{ $error }}</li>
            @endforeach
        </ul>
      </div><br />
    @endif
    @foreach($course as $course)
      <form method="post" action="{{ route('course.update',$course->id) }}">
          @csrf
             <div class="form-group">              
                  <label for="title">College Name:</label>
                  <input type="text" class="form-control" name="college_name" required value="{{ $course->college_name }}" />
                
             </div>  

            <div>
              <label for="Stream">Stream :</label>
              <input type="text" class="form-control" name="stream" required value="{{ $course->stream }}" />
          </div>
          <div>
              <label for="course">Course :</label>
              <input type="text" class="form-control" name="course" required value="{{ $course->course }}" />
          </div>
          <div>
              <label for="cost">Cost :</label>
              <input type="text" class="form-control" name="cost" required value="{{ $course->cost }}" />
          </div>
          <div class="form-group">
              <label for="duration">Duration:</label>
              <input type="text" class="form-control" name="duration" required value="{{ $course->duration }}" />
          </div> 
          <div class="form-group">
              <label for="eligibility">Eligibility:</label>
              <input type="tel" class="form-control" name="eligibility" required value="{{ $course->eligibility }}" />
          </div>
          <div class="form-group">
              <label for="address">address:</label>
              <textarea type="text" class="form-control" name="address" required >{{ $course->address }}</textarea>
          </div>
          <div class="form-group">
              <label for="phone_no">Phone no:</label>
              <input type="text" class="form-control" name="phone_no" required value="{{ $course->phone_no }}" />
          </div> 
          <div class="form-group">
              <label for="web_url">Website :</label>
              <input type="text" class="form-control" name="web_url" required value="{{ $course->web_url }}" />
          </div>         
          <button type="submit" class="btn btn-primary">Update</button>
      </form>
      @endforeach
  </div>
</div>
</div>
@endsection
