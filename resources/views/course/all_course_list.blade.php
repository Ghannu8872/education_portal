@extends('layout')
@section('content')
<div class="container">

    <div class="row ">
    @isset($message)
    <div class="alert alert-success">
    <strong>{{$message}}</strong>
    </div>
    @endif

        <div class="col-sm-12" >
          <div class="row edit-course-box" >
            <div class="col-sm-12" >
             <h1>All Courses </h1>
            <a class="btn btn-success" href="/add-course-detail">Add Course</a> 
            </div>
            <div class="col-sm-12">
             <table border=1>
           
             <tr>
                <th class="td-padding-course-detail col-width">College name</th>
                <th class="td-padding-course-detail col-width">Stream</th>
                <th class="td-padding-course-detail col-width">Course</th>
                <th class="td-padding-course-detail col-width">Duration</th>
                <th class="td-padding-course-detail col-width">Cost</th>
                <th class="td-padding-course-detail col-width">Eligibility</th>
                <th class="td-padding-course-detail col-width">Address</th>
                <th class="td-padding-course-detail col-width">Phone no</th>
                <th class="td-padding-course-detail col-width">Website</th>
                <th class="td-padding-course-detail col-width" colspan=2 >Action</th>
             </tr>
             @foreach($course as $course)
             <tr>
                 <td class="td-padding-course-detail">{{ $course->college_name }} </td>
                <td class="td-padding-course-detail"> {{ $course->stream }} </td>
                <td class="td-padding-course-detail"> {{ $course->course }}</td>
                <td class="td-padding-course-detail"> {{ $course->duration }}</td>
                <td class="td-padding-course-detail"> {{ $course->cost }}</td>
                <td class="td-padding-course-detail"> {{ $course->eligibility }}</td>   
                <td class="td-padding-course-detail"> {{ $course->address }}</td> 
                <td class="td-padding-course-detail"> {{ $course->phone_no }}</td>              
                <td class="td-padding-course-detail"> {{ $course->web_url }}</td> 
                <td class="td-padding-course-detail">
                <a href="{{ route('course.edit',$course->id)}}" class="btn btn-primary">Edit</a>
                </td>
                <td>
                <form action="{{ route('course.delete', $course->id)}}" method="post">
                  @csrf
                  {{ method_field('POST') }} 
                  <button class="btn btn-danger" type="submit">Delete</button>
                </form>
                 </td>
             </tr>            
             @endforeach
             </table>
            </div>
          </div>
        </div>
    </div>  
 </div>
@endsection()