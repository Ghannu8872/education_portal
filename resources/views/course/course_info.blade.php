
@extends('layout')

@section('content')
<div class="container">

    <div class="row ">
        <div class="col-sm-3" >        
            <div class="row">
                  <div class="col-sm-12 sidebar">
                     <ul type="none">
                     <li><h6 class="top_margin">Related Colleges</h6></li>
                     @foreach($clg_list as $clg_list)
                        <li><a href="/#">{{ $clg_list->college_name }}</a> </li>
                        @endforeach  
                     </ul>
                  </div>
           </div>
        </div>
        <div class="col-sm-9" >
        <div  class="row">
             <div class="col-sm-12" >
             <h3>Course Information</h3>
            </div>
            <div class="col-sm-12 edit-box" >
               <table>
                  {{-- @foreach($course_info as $course_info) --}}
                  
                  <tr>
                     <th>College name</th>
                     <td style="color:blue; font-weight: bold;}">{{ $course_info->college_name }}</td>
                  </tr>
                  <tr>
                     <th>Stream</th>
                     <td> {{ $course_info->stream }} </td>
                  </tr>
                  <tr>
                     <th>Duration</th>
                     <td> {{ $course_info->duration }} </td>
                  </tr>
                  <tr>
                     <th>Cost</th>
                     <td> {{ $course_info->cost }} </td>
                  </tr>
                  <tr>
                     <th>Eligibility</th>
                     <td> {{ $course_info->eligibility }} </td>
                  </tr>
                  <tr>
                     <th>Address</th>
                     <td> {{ $course_info->address }} </td>
                  </tr>
                  <tr>
                     <th>Phone no</th>
                     <td> {{ $course_info->phone_no }}</td>
                  </tr>
                  <tr>
                     <th>Website</th>
                     <td> {{ $course_info->web_url }}</td>
                  </tr>
                  {{-- @endforeach --}}
                  </table>
            </div>
        </div>


        </div>
    </div>   
 </div>


@endsection()




{{-- 









@extends('welcome')

@section('content')
<div class="container">
 @include('include/search')
    <div class="row ">
        <div class="col-sm-3" >        
            <div class="row">
                  <div class="col-sm-12 sidebar">
                     <ul type="none">
                        <li><h6 class="top_margin">Related Colleges</h6></li>
                        @foreach($clg_list as $clg_list)
                         <li><a href="/#">{{ $clg_list->college_name }}</a> </li>
                           @endforeach  
                     </ul>
                   </div>
              
           </div>
        </div>
        <div class="col-sm-9" >
        <div  class="row">
             <div class="col-sm-12" >
             <h3>Sponsored Link</h3>
            </div>
            <div class="col-sm-12">
               <!-- @dd($course_info); -->
            <table>
             @foreach($course_info as $course_info)
             
             <tr>
                <th>College name</th>
                <td style="color:blue; font-weight: bold;}">{{ $course_info->college_name }}</td>
             </tr>
             <tr>
                <th>Stream</th>
                <td> {{ $course_info->stream }} </td>
             </tr>
             <tr>
                <th>Duration</th>
                <td> {{ $course_info->duration }} </td>
             </tr>
             <tr>
                <th>Cost</th>
                <td> {{ $course_info->cost }} </td>
             </tr>
             <tr>
                <th>Eligibility</th>
                <td> {{ $course_info->eligibility }} </td>
             </tr>
             <tr>
                <th>Address</th>
                <td> {{ $course_info->address }} </td>
             </tr>
             <tr>
                <th>Phone no</th>
                <td> {{ $course_info->phone_no }}</td>
             </tr>
             <tr>
                <th>Website</th>
                <td> {{ $course_info->web_url }}</td>
             </tr>
             @endforeach
             </table>

            </div>
        </div>
      </div>
      </div>
    </div>
</div>
 
    
@endsection() --}}