<!DOCTYPE html>
<html lang="{{ str_replace('_', '-', app()->getLocale()) }}">
<head>
  <meta charset="utf-8">
  <title>Reveal Bootstrap Template</title>
  <meta content="width=device-width, initial-scale=1.0" name="viewport">
  <meta content="" name="keywords">
  <meta content="" name="description">
  <meta name="csrf-token" content="{{ csrf_token() }}">

  <link rel="stylesheet" href="https://maxcdn.bootstrapcdn.com/bootstrap/3.4.1/css/bootstrap.min.css">
  <script src="https://ajax.googleapis.com/ajax/libs/jquery/3.4.1/jquery.min.js"></script>
  <script src="https://maxcdn.bootstrapcdn.com/bootstrap/3.4.1/js/bootstrap.min.js"></script>
  
  <!-- Favicons -->
  <link href="/img/favicon.png" rel="icon">
  <link href="/img/apple-touch-icon.png" rel="apple-touch-icon">

  <!-- Google Fonts -->
  <link href="https://fonts.googleapis.com/css?family=Open+Sans:300,300i,400,400i,700,700i|Raleway:300,400,500,700,800|Montserrat:300,400,700" rel="stylesheet">

  <!-- Bootstrap CSS File -->
  <link href="/lib/bootstrap/css/bootstrap.min.css" rel="stylesheet">

  <!-- Libraries CSS Files -->
  <link href="/lib/font-awesome/css/font-awesome.min.css" rel="stylesheet">
  <link href="/lib/animate/animate.min.css" rel="stylesheet">
  <link href="/lib/ionicons/css/ionicons.min.css" rel="stylesheet">
  <link href="/lib/owlcarousel/assets/owl.carousel.min.css" rel="stylesheet">
  <link href="/lib/magnific-popup/magnific-popup.css" rel="stylesheet">
  <link href="/lib/ionicons/css/ionicons.min.css" rel="stylesheet">

  <!-- Main Stylesheet File -->
  <link href="/css/style.css" rel="stylesheet">


  <!-- =======================================================
    Theme Name: Reveal
    Theme URL: https://bootstrapmade.com/reveal-bootstrap-corporate-template/
    Author: BootstrapMade.com
    License: https://bootstrapmade.com/license/
  ======================================================= -->

  
  <script src="https://ajax.googleapis.com/ajax/libs/jquery/3.1.0/jquery.min.js"></script>
  <link rel="stylesheet" href="https://maxcdn.bootstrapcdn.com/bootstrap/3.3.6/css/bootstrap.min.css" />
  <script src="https://maxcdn.bootstrapcdn.com/bootstrap/3.3.7/js/bootstrap.min.js"></script>
  <style type="text/css">
   .box{
    width:600px;
    margin:0 auto;
    border:1px solid #ccc;
   }
  </style>
<script  type="text/javascript">
  // $(function() {
  //     $(#collegeform).on('submit', function(e){
  //         e.preventDefault();
  //         var collegeform = $(this);
 
  //         $.ajax({
  //            type : 'post',
  //            url : collegeform.attr('action'),
  //            data: collegeform.serialize(),
  //            success:function(res){
  //           consol.log(res);
  //            }
  //            });
  //     });
  // });
 

//  function submit_form(){
//   var collegeID = $("select[name=college_id]").val(); 
//   var cityID = $("select[name=city]").val();   
//   console.log(collegeID, cityID); 
//      alert(collegeID);
   
//  }


 
 </script>



</head>

<body id="body">


<!--==========================
    Header
  ============================-->
  <header id="header">
  @include('include/header') 
  </header>
  {{-- @include('include/search')  --}}

<!--==========================
    Main
  ============================-->

  <main id="main">

    
 
     <!--==========================
      Our Portfolio Section
    ============================-->
    <section id="portfolio">
     
      <div class="container-fluid">
        <div class="row no-gutters">
          <div class="col-lg-12 col-md-12">

              @yield('content')

          </div>
        </div>
      </div>
    </section><!-- #portfolio -->

     <!--==========================
      Call To Action Section
    ============================-->
    
  </main>

  



 
  

  <!-- JavaScript Libraries -->
  @yield('script')
  <script src="/lib/jquery/jquery.min.js"></script>
  <script src="/lib/jquery/jquery-migrate.min.js"></script>
  <script src="/lib/bootstrap/js/bootstrap.bundle.min.js"></script>
  <script src="/lib/easing/easing.min.js"></script>
  <script src="/lib/superfish/hoverIntent.js"></script>
  <script src="/lib/superfish/superfish.min.js"></script>
  <script src="/lib/wow/wow.min.js"></script>
  <script src="/lib/owlcarousel/owl.carousel.min.js"></script>
  <script src="/lib/magnific-popup/magnific-popup.min.js"></script>
  <script src="/lib/sticky/sticky.js"></script>

  <!-- Contact Form JavaScript File -->
  <script src="/contactform/contactform.js"></script>

  <!-- Template Main Javascript File -->
  <script src="/js/main.js"></script>

  <script type="text/javascript">

    // $.ajaxSetup({
    //             headers:
    //             { 'X-CSRF-TOKEN': $('meta[name="csrf-token"]').attr('content') }
    //         });
    
    //  Get State List
        $('#country').change(function(){
            var old_countryID = '{{ old('countryID') }}';
                if(old_countryID !== '') {
                $('#country').val(old_countryID );
                }
        var countryID = $(this).val();  
        // alert(countryID);
        if(countryID){
            $.ajax({
               type:"GET",
               url:"{{url('get-state-list')}}?country_id="+countryID,
               success:function(res){               
                if(res){
                    $("#state").empty();
                    var options ="<option value='' selected>-Select State-</option>";
                    $.each(res,function(key,value){
                        options ='<option value="'+key+'" >'+value+'</option>';
                       
                        $("#state").append(options);
                    });
               
                }else{
                   $("#state").empty();
                 
                }
               }
            });
        }else{
            $("#state").empty();
            $("#city").empty();
        }      
       });
    
       //  Get City List
        $('#state').on('change',function(){
    
    
        var stateID = $(this).val();    
        if(stateID){
            $.ajax({
               type:"GET",
               url:"{{url('get-city-list')}}?state_id="+stateID,
               success:function(res){               
                if(res){
                    $("#city").empty();
                    var options ="<option value='' selected>-Select State-</option>";
                    $.each(res,function(key,value){
                        options ='<option value="'+key+'" >'+value+'</option>';
                        $("#city").append(options);
                    });
               
                }else{
                   $("#city").empty();
                }
               }
            });
        }else{
            $("#city").empty();
        }
            
       });
    //  Get College List
       $('#city').on('change',function(){
        
        var cityID = $(this).val();    
        if(cityID){
            $.ajax({
               type:"GET",
               url:"{{url('get-college-list')}}?city_id="+cityID,
               success:function(res){               
                if(res){
                    $("#college_id").empty();     
                    var options ="<option value='' selected>-Select College-</option>";         
                    $.each(res,function(key,value){   
                        options ='<option value="'+key+'" >'+value+'</option>';
                        $("#college_id").append(options);    
                  
                    });
               
                }else{
                   $("#city").empty();
                }
               }
            });
        }else{
            $("#city").empty();
        }
            
       });
    
    
    //  Submit Function
    //  $(".btn-submit").click(function(e){
    
    //  e.preventDefault();
     
    // var collegeID = $("select[name=search]").val();
    
    // var password = $("select[name=city]").val();
      
    //             $.ajax({
    //                 type:'GET',
    //                 url:"{{url('get-college-list')}}?college_id="+collegeID,    
    
    //                 success:function(data){
    //                     // location.href = "http://127.0.0.1:8000/result",
    //                     $('tbody').html(data);
    //             }
    //             });
    // });
    
     
    
    
    
    //    $('#search').on('keyup',function(){
    // var value=$(this).val();
    
    // $.ajax({
    // type : 'get',
    // url : '{{URL::to('search')}}',
    // data:{'search':$value},
    // success:function(data){
    // $('tbody').html(data);
    // }
    // });
    // })
    // $(document).ready(function(){
    //     $('#getRequest').click(function(){
    //         var city_id = $("select[name=city]").val();
    //       var  college_id = $("select[name=college_id]").val();
    //     var   dataString= "city_id="+city_id+"&college_id="+college_id;
            // alert("name:"+ city_id + " college_id: "+ college_id );
        //    $.get('getRequest', function(data){
        //        $('#getRequestData').append(data);
        //     console.log(data);
        // $.ajax({
        //     type: "GET",
        //     url: "result",
        //     data: dataString,
        //     success: function(data){
        //         console.log(data);
        //         $('#postRequestData').html(data);
        //     }
        // });
    
        //    });
        
        //    $('#register').submit(function(){
        //    var city_id = $("select[name=city]").val();
        //   var  college_id = $("select[name=college_id]").val();
        //     alert("name:"+ city_id + " college_id: "+ college_id );
        //  var   dataString= "city_id="+city_id+"&college_id="+college_id;
        //  alert(dataString);
        //    $.post('register',{ cityID:city_id, collegeID:college_id}, function(data){
        //        $('#postRequestData').html(data);
        //     console.log(data);
        //    });
        
        // $.ajax({
        //     type: "post",
        //     url: "register",
        //     data: dataString,
        //     success: function(data){
        //         console.log(data);
        //         $('#postRequestData').html(data);
        //     }
        // });
        
        // });
    
        // });
    
    
    
    
    
    </script>
</body>
</html>