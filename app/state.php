<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class state extends Model
{
    public  $table = "state";
    protected $fillable=[

       'country_id',
        'state',
        'vehicle_code'      
    ];
}
