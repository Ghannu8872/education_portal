<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class country extends Model
{   
    public  $table = "country";
    protected $fillable=[

        "country_name",
        'deleted_at'      
    ];
   
}
