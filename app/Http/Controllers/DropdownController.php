<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use DB;
use App\college;
use App\course_detail;
class DropdownController extends Controller
{
    
        public function index()
        {
            $countries = DB::table("country")->pluck("country_name","id");
            return view('pages/index',compact('countries'));
        }
        public function front()
        {
            
            return view('front_layout');
        }

        // public function getCountryList(Request $request)
        // {
           
        //     $countries = DB::table("country")
        //     ->pluck("country_name","id");
        //     dd($countries);
        //     return view('pages/search',compact('countries'));
        // }


        public function getStateList(Request $request)
        {
           
            $states = DB::table("state")
            ->where("country_id",$request->country_id)
            ->pluck("state","id");
            return response()->json($states);
        }

        public function getCityList(Request $request)
        {
            $cities = DB::table("city")
            ->where("state_id",$request->state_id)
            ->pluck("city","id");
            return response()->json($cities);
        }

        public function getCollegeList(Request $request)
        {          
           
                $collegel_list = DB::table("college")
                ->where("city_id",$request->city_id)                
                ->pluck("college_name","id");
              
                return response()->json($collegel_list);
            
        }
       
        public function getCollegeName(Request $request)
        {          
                $collegel_list = DB::table("college")
                ->where("city_id",$request->college_id)
                ->pluck("college_name","id");
                return response()->json($collegel_list);
            
        }

        // public function commerce_course( Request $request )
        // {
        //     dd($request);
        //                 $output="";
    
        //                 $course_list = course_detail::where(function ($query) use ($request)
        //                    {
    
                           
        //                      if (!empty($request->courses)) {
        //                         $query->Where('stream', 'LIKE', '%' . $request->courses . '%');
        //                       }
    
        //                 });
        //                  if($course_list)
        //                 {
        //                 foreach ($course_list as $key => $course_lists)
        //                 {
    
        //                 $output.=
    
        //                 '<li>'.$course_lists->course.'</li>';
        //             }
        //                 }
        //                 return Response($output);
           
        // }

        public function result(Request $request)
            {
               
                $countries = DB::table("country")->pluck("country_name","id");
               $college_id= $request->college_id;
               $city_id= $request->city;
              
               $detail = DB::table("college")
            ->where("id",$college_id)
            ->get();

            $clg_list = DB::table("college")
            ->where("city_id",$city_id)
            ->get();
              
               return view("pages.result", compact('detail','clg_list','countries'));
           
            }


            public function college_detail($city_id, $college_id)
            {
               
                $countries = DB::table("country")->pluck("country_name","id");
               
               $clg_detail = DB::table("college")
            ->where("id",$college_id)
            ->get();

            $clg_list = DB::table("college")
            ->where("city_id",$city_id)
            ->get();

            $commerce_list = DB::table("course_detail")
            ->where("city_id",$city_id)
            ->where("college_id",$college_id)
            ->where("stream",'commerce')
            ->get();

            $management_list = DB::table("course_detail")
            ->where("city_id",$city_id)
            ->where("college_id",$college_id)
            ->where("stream",'management')
            ->get();
                        
               return view("pages.college_detail", compact('clg_detail','clg_list','countries','commerce_list','management_list'));
          }

          public function course_edit($id)
          {
              $course = DB::table("course_detail")->where("id", $id)->get(); 
           
              return view('course.edit_stream', compact('course'));  
          }

          public function course_update(Request $request, $id){
             
            $request->validate([
                'college_name'=>'required',
                'stream'=>'required',
                'course'=>'required',
                'cost'=>'required',
                'duration'=>'required',
                'eligibility' => 'required',
                'phone_no' => 'required',    
                'web_url' => 'required|email' 
             ]);
          $course_data = course_detail::find($id);
         
          $course_data->college_name = $request->get('college_name');
          $course_data->stream = $request->get('stream');
          $course_data->course = $request->get('course');
          $course_data->cost = $request->get('cost');
          $course_data->duration = $request->get('duration');
          $course_data->eligibility = $request->get('eligibility');
          $course_data->address = $request->get('address');
          $course_data->phone_no = $request->get('phone_no');
          $course_data->web_url = $request->get('web_url');
          $message = "Contact Updated!";
          $course_data->save();
          return redirect('/all-course-list')->with('success', 'message');
          }


          public function course_destroy($id)
          {
            $course_data = course_detail::find($id);
              $course_data->delete();
      
              return redirect('/all-course-list')->with('success', 'Contact deleted!');
          }

            //Course
            public function course_info($id)
            {
                $countries = DB::table("country")->pluck("country_name","id");
               
                $course_info = DB::table("course_detail")
                ->where("id",$id)
                ->first();

                $city_id=$course_info->city_id;
                // dd($city_id);
                $clg_list = DB::table("college")
                ->where("city_id",$city_id)
                ->get();
                
                return view("course.course_info",compact('countries','course_info','clg_list'));

            }

            public function add_course_detail_page()
            {
                $countries = DB::table("country")->pluck("country_name","id");
               
                return view("course.add_course_detail",compact('countries'));

            }

            public function all_course_list_page()
            {
                $countries = DB::table("country")->pluck("country_name","id");
                $course = DB::table("course_detail")->get();               
                return view("course.all_course_list",compact('course','countries'));
            }

            public function Store_course_detail(Request $request)
            {
               
                $college_detail = DB::table("college")
                ->where('id',$request->college_id)->first(); 
                $college_name= $college_detail->college_name;               
                // dd($request);
                $request->validate([
                    'college_id'=>'required',                    
                    'city'=>'required',
                    'stream'=>'required',
                    'course'=>'required',
                    'cost'=>'required',
                    'duration'=>'required',
                    'eligibility' => 'required',
                    'phone_no' => 'required|max:12',    
                    'web_url' => 'required|email' 
                 ]);
                  dd($request);
                $course_data = new course_detail([                    
                    'college_id' => $request['college_id'],
                    'college_name' =>  $college_name,
                    'city_id' => $request['city'],
                    'stream' => $request['stream'],
                    'course' => $request['course'],
                    'cost' => $request['cost'],
                    'duration' => $request['duration'],
                    'eligibility' => $request['eligibility'],
                    'address' => $request['address'],
                    'phone_no' => $request['phone_no'],
                    'web_url' => $request['web_url']
                ]);
            //    dd($course_data);
                $course_data->save();
                $message = "Data Saved!";
                return redirect('/all-course-list')->with('success', 'message');
              
            }

            // college
            public function add_college_page()
            {
                $countries = DB::table("country")->pluck("country_name","id");
               
                return view("pages.add_college_detail",compact('countries'));

            }
            public function all_college_list_page()
            {
                $countries = DB::table("country")->pluck("country_name","id");
                $colleges = DB::table("college")->get();               
                return view("pages.all_college_list",compact('colleges','countries'));
            }
            public function college_edit($id)
            {
                $colleges = DB::table("college")->where("id", $id)->get(); 
                // $college = college::find($id);
                return view('pages.college-edit', compact('colleges'));  
            }

            public function college_update(Request $request, $id){
               
                $request->validate([                     
                   
                    'college_name'=>'required',
                    'address'=>'required',
                    'phone_no' => 'required',    
                    'web_url' => 'required|email' ,
                    'university' => 'required',   
                    'contact_person' => 'required',
                   
                 ]);
            $college_data = college::find($id);
           
            $college_data->college_name = $request->get('college_name');
            $college_data->address = $request->get('address');
            $college_data->phone_no = $request->get('phone_no');
            $college_data->web_url = $request->get('web_url');
            $college_data->university = $request->get('university');
            $college_data->contact_person = $request->get('contact_person');
            $message = "Contact Updated!";
            $college_data->save();
            return redirect('/all-college-list')->with('success', 'message');
            }
            public function college_destroy($id)
            {
                $college = college::find($id);
                $college->delete();
        
                return redirect('/all-college-list')->with('success', 'Contact deleted!');
            }

            

            public function Store_college(Request $request)
            {
                // dd($request);
                // $request->validate([
                //     'first_name'=>'required|alpha|min:3|max:255|Regex:/^[\D]+$/i',
                //     'last_name'=>'required|min:3|max:255',
                //     'user_name'=>'required|min:3|max:255|alpha_num',
                //     'email'=>'required|email|unique:contacts',
                //     'Mob_no'=>'required|numeric',
                //     'password' => 'required|min:3|max:20',
                //     'confirm_password' => 'required|same:password',    
                //     'zipcode' => 'required|numeric|min:6' 
                // ]);
                $request->validate([
                                       
                    'city'=>'required',
                    'college_name'=>'required',
                    'address'=>'required',
                    'phone_no' => 'required',    
                    'web_url' => 'required|email' ,
                    'university' => 'required',   
                    'contact_person' => 'required',
                   
                 ]);
                $college_data = new college([
                   
                    'city_id' => $request['city'],
                    'college_name' =>$request['college_name'],
                    'address' =>$request['address'] ,
                    'phone_no' => $request['phone_no'],
                    'web_url' =>$request['web_url'],                    
                    'university' =>$request['university'],
                    'contact_person' =>$request['contact_person']
                ]);
                // dd($college_data);
                $college_data->save();
                $message = "Data Saved!";
                return redirect('/all-college-list')->with('success', 'message');
              
            }


            

        //     public function index1(){
        //         return view('pages.autocomplete');
        //    }
        //     public function autoComplete(Request $request) {
        //         $query = $request->get('term','');
                
        //         $products=Product::where('name','LIKE','%'.$query.'%')->get();
                
        //         $data=array();
        //         foreach ($products as $product) {
        //                 $data[]=array('value'=>$product->name,'id'=>$product->id);
        //         }
        //         if(count($data))
        //              return $data;
        //         else
        //             return ['value'=>'No Result Found','id'=>''];
        //     }

        // public function showcollege(Request $request)
        // {     
        //     $college_detail = DB::table("college")
        //     ->where("id",$request->college_id)
        //     ->get();
        //     return response()->json($college_detail);            
        // }
        
        
            
        // public function result1(Request $request)
        //     {
             
        //     if($request->ajax())
        //         {
        //         $output="";
        //         $College=DB::table('college')->where('id','LIKE','%'.$request->college_id."%")->get();
        //         if($College)
        //         {
                   
        //             foreach ($College as $key => $College) {
        //             $output.='<tr>'.
        //             '<td>'.$College->college_name.'</td>'.
        //             '<td>'.$College->address.'</td>'.
        //             '<td>'.$College->phone_no.'</td>'.
        //             '<td>'.$College->web_url.'</td>'.
        //             '<td>'.$College->university.'</td>'.
        //             '<td>'.$College->contact_person.'</td>'.
        //             '</tr>';
        //         }
        //           return Response($output);
        //         }
        //         }
        //     }

}
