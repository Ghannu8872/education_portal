<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class college extends Model
{
    public  $table = "college";
    protected $fillable=[

        'city_id',
        'college_name',
        'address',
        'phone_no',
        'web_url',
        'university',
        'contact_person',
        'update',

    ];
}
