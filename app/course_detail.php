<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class course_detail extends Model
{
    public  $table = "course_detail";
    protected $fillable  = [
        'college_id',
        'college_name',
        'city_id',
        'stream',
        'course',
        'duration',
        'cost',
        'eligibility',
        'address',
        'phone_no',
        'web_url',
    ];
    
}
