<?php

/*
|--------------------------------------------------------------------------
| Web Routes
|--------------------------------------------------------------------------
|
| Here is where you can register web routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| contains the "web" middleware group. Now create something great!
|
*/

Route::get('/front','DropdownController@front');

Route::get('/','DropdownController@index');

Route::get('get-country-list','DropdownController@getCountryList');
Route::get('get-state-list','DropdownController@getStateList');
Route::get('get-city-list','DropdownController@getCityList');
Route::get('get-college-list','DropdownController@getCollegeList');
Route::get('get-college-name','DropdownController@getCollegeName');
Route::get('get-commerce-course','DropdownController@commerce_course');


//create page
Route::get('add-college','DropdownController@add_college_page');
Route::get('add-course-detail','DropdownController@add_course_detail_page');

//edit
Route::get('/college-edit/{id}',[ 'as' => 'clg.edit', 'uses' => 'DropdownController@college_edit'] );
Route::get('/course-edit/{id}',[ 'as' => 'course.edit', 'uses' => 'DropdownController@course_edit'] );
//delete
Route::POST('/college-delete/{id}',[ 'as' => 'clg.delete', 'uses' => 'DropdownController@college_destroy'] );
Route::POST('/course-delete/{id}',[ 'as' => 'course.delete', 'uses' => 'DropdownController@course_destroy'] );
//update
Route::POST('/college-update/{id}',[ 'as' => 'clg.update', 'uses' => 'DropdownController@college_update'] );
Route::POST('/course-update/{id}',[ 'as' => 'course.update', 'uses' => 'DropdownController@course_update'] );

//show
Route::get('all-college-list','DropdownController@all_college_list_page'); 
Route::get('all-course-list','DropdownController@all_course_list_page');
Route::get('/college_detail/{city_id}/{college_id}','DropdownController@college_detail');
Route::get('/course-detail/{id}',[ 'as' => 'course.info', 'uses' => 'DropdownController@course_info'] );
Route::POST('/result',[ 'as' => 'clg.result', 'uses' => 'DropdownController@result'] );
//store
Route::POST('/store_college',[ 'as' => 'clg.store', 'uses' => 'DropdownController@Store_college'] );
Route::POST('/store_course_detail',[ 'as' => 'course.store', 'uses' => 'DropdownController@Store_course_detail'] );

// Route::get('get-college-list1','DropdownController@result1');



Route::get('autocomplete',array('as'=>'autocomplete','uses'=>'DropdownController@index1'));
Route::get('searchajax',array('as'=>'searchajax','uses'=>'DropdownController@autoComplete'));


Route::get('/getRequest',function(){
        if(Request::ajax()){
            return "getRequest has loaded completed";
        }
});

Route::post('/register',function(){
    if(Request::ajax()){
        return Response::json(Request::all());
    }
});